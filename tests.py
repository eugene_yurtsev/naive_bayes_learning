from oracle import Oracle
from scipy.stats import bernoulli, uniform, poisson
from numpy import empty
from numpy.random import choice
from itertools import product
import pandas # Import locally in case dependence is missing

## Tests to implement for verifying interface
# 1. raise ValueError when querying to predict probability of past events.
# 2. verify that prior is return for new experts with no predictions (0.5)
# 3. Oracle prediction based on new experts should return event prior (0.5)
# 4. Numerically verify some of the results for a simulation started at a given seed.

# To verify simulation interface
# 3. ask an expert to make a prediction a number of times (answer always be the same, otherwise
# the underlying probability model changes)

class VirtualExpert(object):
    def __init__(self, name):
        self.name = name

        ## dictionary holding information about predictions
        # Keys: events, values: predictions
        self.predictions = {}

    def __repr__(self):
        return '{}'.format(self.name)

class VirtualEvent(object):
    """ Implements VirtualEvents for testing """
    def __init__(self, ID, outcome):
        self.ID = ID
        self._outcome = outcome
        self.happened = False

    def register_outcome(self, outcome='dummy'):
        """ dummy interface to register an event.
        We only need to update the outcome for real events when
        the outcome is really unknown. """
        self.happened = True

    @property
    def outcome(self):
        if self.happened:
            return self._outcome
        else:
            return None

    def __repr__(self):
        if self.outcome is not None:
            outcome = self.outcome
        else:
            outcome = 'unknown'
        return 'Event #{} (outcome={})'.format(self.ID,\
                outcome)

class Demo(object):
    """ Demonsrtates evaluation of experts, acquisition of new
    experts, polling for expert opinion on new events and
    refined forecasting using 'oracle' which weighs expert opinion
    based on past performance. """
    def __init__(self):
        self.experts = []
        self.oracle = Oracle()

        self.expert_list = []
        self.event_list = []

        ##
        # Real expert quality is only available
        # in the simulation
        self.expert_true_quality = {}

    def generate_expert(self, quality_range=(0.5, 0.9)):
        """
        Generates an expert with quality drawn from a uniform distribution
        specified by the range parameter.
        """
        name = 'Expert #{}'.format(len(self.expert_list))
        expert = VirtualExpert(name)
        self.oracle.add_expert(expert)
        self.expert_list.append(expert)

        # Assign a true expertise level from a uniform distribution
        loc = quality_range[0] # starting point
        scale = quality_range[1] - quality_range[0] # width of dist
        true_quality = uniform.rvs(loc=loc, scale=scale)
        self.expert_true_quality[expert] = true_quality
        return expert

    def generate_event(self, p=0.5):
        """ Generates events from a bernoulli process.

        Parameters
        -----------
        p : float
            Specifies the probability of seeing an event of type 1
            The probability of seeing the alternative event is (1-p)
        """
        outcome = int(bernoulli.rvs(p, size=1))
        ID = len(self.event_list)
        event = VirtualEvent(ID, outcome)
        self.event_list.append(event)
        return event

    def ask_an_expert(self, expert, event):
        """ Simulates the process of asking an expert to predict the outcome of an event.

        Note: experts should not change their mind regarding the outcome of an event!
        (In reality they may revise their estimates as more information is available.)
        """
        if event not in expert.predictions:
            quality = self.expert_true_quality[expert]
            answer_correct = bernoulli.rvs(quality)
            # We are allowed to look into the future here
            true_outcome = event._outcome

            if answer_correct:
                prediction = true_outcome
            else:
                prediction = int(not(bool(true_outcome)))

            expert.predictions[event] = prediction

            return prediction
        else:
            return expert.predictions[event]

    def ask_the_oracle(self, event):
        h1_probability = self.oracle.estimate_h1_probability(event)
        prediction = round(h1_probability)

        # Assign a probability to the prediction
        if h1_probability >= 0.5:
            p = h1_probability
        else:
            # The oracle chooses the alternative hypothesis
            p = 1 - h1_probability
        # Look up the real answer to see if the oracle was right
        correct = (prediction == event._outcome)
        return prediction, p, correct

    def get_expert_quality(self, expert):
        """
        Returns 5-tuple with expert quality information
        """
        estimated_quality, num_correct, num_wrong =\
                self.oracle.evaluate_expert(expert)
        return (self.expert_true_quality[expert],
                estimated_quality, num_correct, num_wrong)

def simulate_experts(num_experts, expert_quality_range, num_events, polling_parameter='all'):
    """ Runs a simulation testing the oracle.

    Parameters
    --------------

    num_experts : int
        number of experts to start the simulation with
    expert_quality_range : (float, float)
        range of uniform distribution from which to draw parameters for expert quality
    num_events : int
    polling_parameter : ['all' | float]
        Specifies how many of the experts to poll for opinions
        'all' polls all experts
        float : poisson mean for number of experts to poll
    """
    # Populate database with experts, events

    d = Demo()

    oracle_predictions = {}

    for i in range(num_experts):
        expert = d.generate_expert(expert_quality_range)

    for j in range(num_events):
        event = d.generate_event()

        ##
        # Poll experts for opinions (either randomly or all of them)
        if polling_parameter == 'all':
            num_to_choose = 'all'
        else:
            num_to_choose = poisson.rvs(polling_parameter)
            if num_to_choose >= num_experts:
                num_to_choose = 'all'

        if num_to_choose == 'all':
            chosen_experts = d.expert_list
        else:
            chosen_experts = choice(d.expert_list, num_to_choose, replace=False)

        # poll experts
        for expert in chosen_experts:
            d.ask_an_expert(expert, event)

        oracle_predictions[event] = d.ask_the_oracle(event)
        event.register_outcome() # Record outcome of event

    ##
    # Summarize poll results
    output = [d] # Reference to the state of the demo

    data = empty(shape=(num_experts, num_events), dtype=object)
    data = pandas.DataFrame(data)

    column_names = ['{}'.format(e.ID) for e in d.event_list]
    data.columns = column_names
    data.columns.name = 'Event'
    data.index.name = 'Expert'

    for loc in product(range(num_experts), range(num_events)):
        expert = d.expert_list[loc[0]]
        event = d.event_list[loc[1]]
        prediction = expert.predictions.get(event, None)
        if prediction == None:
            answer = '-'
        else:
            answer = d.oracle.evaluate_prediction(event, prediction)
            answer = 'C' if answer else 'W'
        data.iloc[loc] = answer

    output.append(data.T)

    ##
    # Oracle predictions
    data = empty(shape=(num_events, 3), dtype=object)
    data = pandas.DataFrame(data)
    column_names = ['hypothesis', 'probability', "correct"]
    data.columns = column_names
    data.columns.name = 'Oracle'
    data.index.name = 'Event'

    for i, event in enumerate(d.event_list):
        prediction = oracle_predictions.get(event, None)

        if prediction is not None:
            hypothesis, p, correct = prediction
            p = '{:.4f}'.format(p)
            if correct:
                correct = 'C'
            else:
                correct = 'W'
        else:
            hypothesis, p, correct = '-', '-', '-'

        data.iloc[i] = hypothesis, p, correct
    output.append(data)

    ###
    # Current oracle opinion about experts
    data = empty(shape=(num_experts, 4), dtype=object)
    column_names = ['True Quality', 'Estimated Quality', '# correct', '# wrong']
    data = pandas.DataFrame(data, columns=column_names)
    data.index.name = 'Expert'

    for i, expert in enumerate(d.expert_list):
        data.iloc[i] = d.get_expert_quality(expert)

    output.append(data)
    return output

