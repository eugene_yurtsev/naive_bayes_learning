class Oracle(object):
    """
    The Oracle uses past performance of experts to:
        1. Estimate the quality of experts
        2. Estimates the likelihood of new events based on different
        expert opinions.

    This oracle works for experts that make predictions regarding
    which of two alternative outcomes will occur.
    """
    def __init__(self):
        self.expert_list = []

    def add_expert(self, expert):
        self.expert_list.append(expert)

    def estimate_h1_probability(self, event):
        ## Evaluate the quality of the experts
        # * should re-evaluate only when new information
        # is added about predictions & final outcomes
        # won't implement now
        event_prior = 0.5

        if len(self.expert_list) == 0:
            return event_prior # Best guess absent more information or event prior

        if event.outcome is not None:
            raise ValueError("Cannot estimate event likelihood for" + \
                   "past events that")

        expert_contributions = []

        for expert in self.expert_list:
            q, _, _ = self.evaluate_expert(expert) # q is for quality

            if event in expert.predictions:
                y = expert.predictions[event] # y is the prediction (0 or 1)
                expert_factor = q**y * (1-q)**(1-y) / (q**(1-y) * (1-q)**(y))
                expert_contributions.append(expert_factor)

        if len(expert_contributions) == 0:
            return event_prior
        else:
            probability_ratio = reduce(lambda x, y: x*y, expert_contributions)
            event_probability = probability_ratio / (1.0 + probability_ratio)
            return event_probability

    def evaluate_prediction(self, event, prediction):
        return event.outcome == prediction

    def evaluate_expert(self, expert):
        """ Estimates the quality of an expert using a laplacian estimator. """
        predictions = expert.predictions

        data = [(event.outcome, prediction) for event, prediction in
                predictions.iteritems() if event.outcome is not None]

        if len(data) > 0:
            num_events = len(data)
            num_correct = sum([x[0] == x[1] for x in data])
            num_wrong = num_events - num_correct
        else:
            # The absence of a prediction history, laplacian estimator
            # should return 0.5
            num_correct, num_wrong = 0, 0
        laplacian_estimate = float(num_correct + 1) / (num_correct + num_wrong + 2)
        return laplacian_estimate, num_correct, num_wrong

    def rate_expert(self, expert):
        """
        Returns a star rating for each expert.

        An obvious thing to implement once we decide on a reasonable
        definition for a star system :)
        """
        raise NotImplementedError
